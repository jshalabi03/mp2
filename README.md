# Space Data Explorer

Frontend interface for interacting with the [Launch Library 2 public API](https://thespacedevs.com/llapi).

## Built with

- React
- Vite
- TypeScript
- TailwindCSS
- Radix UI
- React Query
- React Router v6
- Zod
