import { navItems } from "@/config/nav-items";
import { cn } from "@/lib/utils";
import { NavLink } from "react-router-dom";

export function MainHeader() {
  return (
    <div className="pt-8 self-center text-center">
      <div className="px-4 sm:px-6 md:px-8">
        <h2 className="text-3xl sm:text-5xl font-semibold text-gray-900 ">
          Space Explorer 🚀
        </h2>
      </div>
      <div className="px-4 sm:px-6 md:px-8 mt-4">
        <p className="text-md text-gray-500">
          Explore all data about space via the{" "}
          <a
            className="text-indigo-400 border-b border-indigo-400 hover:text-indigo-600 hover:border-indigo-600"
            target="_blank"
            href="https://thespacedevs.com/llapi"
          >
            Launch Library 2 API
          </a>
        </p>
      </div>
      <div className="px-4 sm:px-6 md:px-8 mt-4">
        <div className="border-b border-gray-200">
          <nav className="flex flex-col items-center sm:-mb-px sm:flex-row sm:space-x-8 sm:justify-center">
            {navItems.map((item, idx) => (
              <NavLink
                key={idx}
                to={item.path}
                className={({ isActive }) =>
                  cn(
                    isActive
                      ? "border-indigo-500 text-indigo-600"
                      : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300",
                    "inline-flex px-1 pt-1 border-b-2 text-lg font-medium"
                  )
                }
              >
                {item.label}
              </NavLink>
            ))}
          </nav>
        </div>
      </div>
    </div>
  );
}
