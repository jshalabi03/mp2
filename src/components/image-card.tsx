import { ArrowRight } from "lucide-react";
import { Link, useSearchParams } from "react-router-dom";

interface ImageCardProps {
  children?: React.ReactElement;
  className?: string;
  imageSrc: string;
  title: string;
  description?: string;
  detailUrl?: string;
}

export function ImageCard({
  children,
  className,
  imageSrc,
  title,
  description,
  detailUrl,
}: ImageCardProps) {
  const [searchParams] = useSearchParams();
  return (
    <div className={`rounded-lg shadow-lg overflow-hidden ${className}`}>
      <img className={`w-full h-56 object-cover`} src={imageSrc} alt={title} />
      <div className="p-6">
        <h2 className="text-2xl font-bold">{title}</h2>
        {description ? (
          <p className="text-sm text-gray-500 my-2 max-h-20 overflow-y-auto">
            {description}
          </p>
        ) : null}
        {children}
        {detailUrl ? (
          <div className="mt-4">
            <Link
              className="inline-flex items-center justify-center p-2 text-base font-medium rounded-lg text-white bg-gray-800 hover:bg-gray-700 hover:text-white"
              to={{ pathname: `${detailUrl}`, search: searchParams.toString() }}
            >
              See More
              <ArrowRight className="ml-2 h-4 w-4" />
            </Link>
          </div>
        ) : null}
      </div>
    </div>
  );
}
