import { Sliders } from "lucide-react";
import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { Popover, PopoverContent, PopoverTrigger } from "./ui/popover";
import { Field } from "@/config/views";

interface FilterPopoverProps {
  filterOptions: Field[];
  selectedFilterValues: string[];
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

export function FilterPopover({
  filterOptions,
  onChange,
  selectedFilterValues,
}: FilterPopoverProps) {
  return (
    <Popover>
      <PopoverTrigger asChild>
        <Button
          className="relative inline-flex items-center px-2.5 py-2.5 text-sm font-medium text-center rounded-lg"
          variant="outline"
          size="icon"
        >
          <Sliders className="h-4 w-4" />
          {selectedFilterValues.length ? (
            <div className="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-2 -right-2 dark:border-gray-900">
              {selectedFilterValues.length}
            </div>
          ) : null}
        </Button>
      </PopoverTrigger>
      <PopoverContent className="w-80">
        <h3 className="font-bold border-b border-black w-fit">Filter</h3>
        <div className="flex mt-4">
          {filterOptions.map((option, index) => (
            <div key={index} className="flex flex-row space-x-2">
              <Input
                id={option.field}
                checked={selectedFilterValues?.includes(option.field)}
                type="checkbox"
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500"
                onChange={onChange}
              />
              <Label htmlFor={option.field}>{option.label}</Label>
            </div>
          ))}
        </div>
      </PopoverContent>
    </Popover>
  );
}
