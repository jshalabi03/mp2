import { Field } from "@/config/views";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "./ui/select";
import { ArrowUp, X } from "lucide-react";
import { Button } from "./ui/button";
import { SortMode } from "@/lib/view-renderer";

interface SortProps {
  selectedSort: string;
  sortMode: SortMode;
  sortFields: Field[];
  onChange?: (value: string) => void;
  onSortModeChange?: React.MouseEventHandler<HTMLButtonElement>;
  onSortModeClear?: React.MouseEventHandler<HTMLButtonElement>;
}

export function Sort({
  selectedSort,
  sortMode,
  sortFields,
  onChange,
  onSortModeChange,
  onSortModeClear,
}: SortProps) {
  return (
    <Select onValueChange={onChange} value={selectedSort}>
      <SelectTrigger className="w-[150px]">
        <SelectValue placeholder="Sort" />
      </SelectTrigger>
      <div className="flex flex-row space-x-1">
        <Button
          onClick={onSortModeChange}
          className="ml-2 bg-blue-400 hover:bg-blue-600"
          size="icon"
        >
          <ArrowUp
            className={`h-4 w-4 transition-all duration-200 ${
              sortMode === "asc" ? "rotate-0" : "rotate-180"
            }`}
          />
        </Button>
        <Button
          onClick={onSortModeClear}
          className="bg-red-400 hover:bg-red-600"
          size="icon"
        >
          <X className="h-4 w-4" />
        </Button>
      </div>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>Sort results by</SelectLabel>
          <SelectItem value="no-sort">No Sort</SelectItem>
          {sortFields.map((s) => (
            <SelectItem key={s.field} value={s.field}>
              {s.label}
            </SelectItem>
          ))}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
}
