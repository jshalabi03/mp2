import { ArrowRight } from "lucide-react";
import { Link, useSearchParams } from "react-router-dom";

interface AvatarCardProps {
  children?: React.ReactElement;
  className?: string;
  imageSrc: string;
  title: string;
  description?: string;
  detailUrl?: string;
}

export function AvatarCard({
  children,
  className,
  imageSrc,
  title,
  description,
  detailUrl,
}: AvatarCardProps) {
  const [searchParams] = useSearchParams();
  return (
    <div
      className={`flex items-center space-x-4 p-6 rounded-lg shadow-lg bg-white ${className}`}
    >
      <img
        className="w-48 h-48 rounded-full shadow-lg border-2 border-gray-200"
        src={imageSrc}
        alt={title}
      />
      <div>
        <h2 className="text-xl font-bold">{title}</h2>
        {description ? (
          <p className="text-sm text-gray-500 mt-2">{description}</p>
        ) : null}
        {children}
        {detailUrl ? (
          <div className="mt-4">
            <Link
              className="inline-flex items-center justify-center px-4 py-2 text-base font-medium rounded-lg text-white bg-indigo-600 hover:bg-indigo-500"
              to={{ pathname: `${detailUrl}`, search: searchParams.toString() }}
            >
              See More
              <ArrowRight className="ml-2 h-4 w-4" />
            </Link>
          </div>
        ) : null}
      </div>
    </div>
  );
}
