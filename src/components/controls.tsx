import { ViewObject } from "@/config/views";
import { FilterPopover } from "./filter-popover";
import { Search } from "./search";
import { Sort } from "./sort";
import { SortMode } from "@/lib/view-renderer";

interface ControlsProps {
  view: ViewObject;
  selectedFilterValues: string[];
  selectedSort: string;
  sortMode: SortMode;
  onFilterChange: React.ChangeEventHandler<HTMLInputElement>;
  onSearchChange: React.ChangeEventHandler<HTMLInputElement>;
  onSortChange: (value: string) => void;
  onSortModeChange: React.MouseEventHandler<HTMLButtonElement>;
  onSortModeClear: React.MouseEventHandler<HTMLButtonElement>;
  className?: string;
}

export function Controls({
  className,
  view,
  selectedFilterValues,
  selectedSort,
  sortMode,
  onFilterChange,
  onSearchChange,
  onSortChange,
  onSortModeChange,
  onSortModeClear,
}: ControlsProps) {
  return (
    <div className={className}>
      {view.filterOptions.length ? (
        <FilterPopover
          selectedFilterValues={selectedFilterValues}
          filterOptions={view.filterOptions}
          onChange={onFilterChange}
        />
      ) : null}
      <Search key={view.title} onChange={onSearchChange} />
      {view.orderingFields.length ? (
        <div className="flex flex-row">
          <Sort
            sortMode={sortMode}
            sortFields={view.orderingFields}
            selectedSort={
              sortMode === "asc" ? selectedSort : selectedSort.substring(1)
            }
            onChange={onSortChange}
            onSortModeChange={onSortModeChange}
            onSortModeClear={onSortModeClear}
          />
        </div>
      ) : null}
    </div>
  );
}
