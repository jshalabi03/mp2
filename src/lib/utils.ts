import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function parseSearchParam(urlString: string, searchParam: string) {
  return new URL(urlString).searchParams.get(searchParam);
}
