import React from "react";
import { z } from "zod";
import {
  Await,
  NavigateOptions,
  Outlet,
  useLoaderData,
  useNavigate,
} from "react-router-dom";
import { useInfiniteQuery } from "react-query";
import { ViewObject } from "@/config/views";
import { Spinner } from "@/components/spinner";
import { Button } from "@/components/ui/button";
import { Dialog, DialogContent } from "@/components/ui/dialog";
import { Controls } from "@/components/controls";
import { parseSearchParam } from "./utils";
import { useDebouncedCallback } from "use-debounce";
import useSearchParamState from "./hooks";

export type SortMode = "asc" | "desc";

interface ViewRendererProps {
  view: ViewObject;
}

export const ViewRenderer: React.FC<ViewRendererProps> = ({ view }) => {
  const RESPONSE_LIMIT = 10;
  const DEBOUNCE_VALUE = 1000;

  const ViewComponent = view.dataComponent;

  const [searchParams, setSearchParams] = useSearchParamState();

  const searchQuery = searchParams.get("search") ?? "";
  const selectedFilters = searchParams.get("filter")?.split(",") ?? [];
  const selectedSort = searchParams.get("sort") ?? "no-sort";
  const sortMode: SortMode = selectedSort.length
    ? selectedSort[0] === "-"
      ? "desc"
      : "asc"
    : "asc";

  const setSearchParamsDebounced = useDebouncedCallback(
    (
      key: string,
      value: string | null,
      navigateOpts?: NavigateOptions | undefined
    ) => setSearchParams(key, value, navigateOpts),
    DEBOUNCE_VALUE
  );

  const setSearchQuery: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setSearchParamsDebounced(
      "search",
      e.target.value.length ? e.target.value : null
    );
  };

  const setSelectedFilterOptions: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => {
    const optionIndex = selectedFilters.findIndex(
      (option) => option === e.target.id
    );
    const newFilterOptions = [...selectedFilters];
    if (e.target.checked) {
      if (optionIndex === -1) {
        newFilterOptions.push(e.target.id);
      }
    } else {
      if (optionIndex !== -1) {
        newFilterOptions.splice(optionIndex, 1);
      }
    }
    setSearchParams(
      "filter",
      newFilterOptions.length ? newFilterOptions.join(",") : null
    );
  };

  const setSelectedSortOption = (value: string) => {
    setSearchParams(
      "sort",
      value.length && value !== "no-sort"
        ? `${sortMode === "desc" ? "-" : ""}${value}`
        : null
    );
  };

  const toggleSortMode: React.MouseEventHandler<HTMLButtonElement> = () => {
    if (!selectedSort.length || selectedSort === "no-sort") return;
    setSearchParams(
      "sort",
      sortMode === "asc" ? `-${selectedSort}` : selectedSort.substring(1)
    );
  };

  const clearSortMode: React.MouseEventHandler<HTMLButtonElement> = () => {
    setSearchParams("sort", null);
  };

  const dataFetcher = async ({ pageParam = 0 }) => {
    const defaultParams = `limit=${RESPONSE_LIMIT}&offset=${pageParam}`;
    const filterParams = selectedFilters.length
      ? "&" + selectedFilters.map((option) => `${option}=true`).join("&")
      : "";
    const orderingParam =
      selectedSort.length && selectedSort !== "no-sort"
        ? `&ordering=${selectedSort}`
        : "";
    const searchParam = searchQuery.length ? `&search=${searchQuery}` : "";
    const url = `${view.endpointPath}/?${defaultParams}${searchParam}${filterParams}${orderingParam}`;
    console.log("fetching ", url);
    return fetch(url).then((res) => res.json());
  };

  /**
   * This key uniquely identifies a query for view response data.
   * This key should be the composition of any dynamic
   * dependencies in defining response url for fetcher
   */
  const queryKey: Array<string> = [
    view.title,
    searchQuery,
    `filter-${selectedFilters.join("-")}`,
    `${selectedSort.length ? `${selectedSort}-${sortMode}` : ""}`,
  ];

  const {
    data,
    error,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    status,
  } = useInfiniteQuery(queryKey, dataFetcher, {
    getNextPageParam: (lastPage) =>
      lastPage.next ? parseSearchParam(lastPage.next, "offset") : null,
  });

  return (
    <div className="flex flex-col justify-center items-center m-4 p-6 gap-6">
      <p className="text-lg text-gray-500 my-1">{view.description}</p>
      <Controls
        className="sticky top-2.5 flex flex-row space-x-2 p-2 bg-gray-200 rounded-lg shadow-md"
        view={view}
        sortMode={sortMode}
        selectedSort={selectedSort}
        selectedFilterValues={selectedFilters}
        onFilterChange={setSelectedFilterOptions}
        onSearchChange={setSearchQuery}
        onSortChange={setSelectedSortOption}
        onSortModeChange={toggleSortMode}
        onSortModeClear={clearSortMode}
      />
      {status === "loading" ? (
        <Spinner />
      ) : status === "error" ? (
        <p>Error: {(error as Error).message}</p>
      ) : (
        <div>
          {data && data.pages.length ? (
            <div className="flex flex-col gap-4 justify-center align-center max-w-screen-lg">
              {data.pages.map((group, groupIdx) =>
                group.results.map(
                  (
                    dataItem: z.infer<typeof view.responseSchema>,
                    itemIdx: number
                  ) => (
                    <ViewComponent
                      key={`${groupIdx}-${itemIdx}`}
                      dataItem={dataItem}
                    />
                  )
                )
              )}
              <Button
                onClick={() => fetchNextPage()}
                disabled={!hasNextPage || isFetchingNextPage}
              >
                {isFetchingNextPage ? (
                  <Spinner />
                ) : hasNextPage ? (
                  "Load More"
                ) : (
                  "Nothing more to load"
                )}
              </Button>
            </div>
          ) : (
            <p>No data available, check back later.</p>
          )}
        </div>
      )}
      <Outlet />
    </div>
  );
};

type DetailViewRendererProps = {
  view: ViewObject;
};

export const DetailViewRenderer: React.FC<DetailViewRendererProps> = ({
  view,
}) => {
  const DetailComponent = view.detailComponent;
  const data = useLoaderData();
  const navigate = useNavigate();
  return (
    <Dialog open={true} onOpenChange={(open) => !open && navigate(-1)}>
      <DialogContent className="rounded-lg shadow-lg max-h-[calc(100%-2rem)] h-fit overflow-y-auto justify-center">
        <React.Suspense fallback={<Spinner className="my-6" />}>
          {
            <Await
              resolve={
                (
                  data as {
                    dataItem: z.infer<typeof view.detailSchema>;
                  }
                ).dataItem
              }
              errorElement={
                <p className="text-sm text-gray-500">
                  An unknown error occured!
                </p>
              }
            >
              {(dataItem: z.infer<typeof view.detailSchema>) => (
                <DetailComponent dataItem={dataItem} />
              )}
            </Await>
          }
        </React.Suspense>
      </DialogContent>
    </Dialog>
  );
};
