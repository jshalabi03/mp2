import {
  NavigateOptions,
  URLSearchParamsInit,
  useSearchParams,
} from "react-router-dom";

const useSearchParamState = (defaultInit?: URLSearchParamsInit | undefined) => {
  const [searchParams, setSearchParams] = useSearchParams(defaultInit);

  const setSearchParamState = (
    key: string,
    value: string | null,
    navigateOpts?: NavigateOptions
  ) => {
    if (value == null) {
      searchParams.delete(key);
    } else {
      searchParams.set(key, value);
    }
    setSearchParams(searchParams, navigateOpts);
  };

  return [searchParams, setSearchParamState] as const;
};
export default useSearchParamState;
