import {
  AstronautDetailedSchema,
  AstronautSchema,
} from "@/api/schemas/astronaut";
import { AvatarCard } from "@/components/avatar-card";
import { z } from "zod";
import { DateTime, Duration } from "luxon";

interface AstronautViewProps {
  dataItem: z.infer<typeof AstronautSchema>;
}

export function AstronautView({ dataItem }: AstronautViewProps) {
  return (
    <AvatarCard
      imageSrc={dataItem.profile_image ?? ""}
      title={dataItem.name}
      description={dataItem.bio}
      detailUrl={`${dataItem.id}`}
    >
      <div className="flex flex-row space-x-2 mt-2">
        {dataItem.in_space ? (
          <span className="inline-block bg-blue-200 text-blue-800 p-2 radius">
            In Space
          </span>
        ) : null}
      </div>
    </AvatarCard>
  );
}

interface AstronautDetailViewProps {
  dataItem: z.infer<typeof AstronautDetailedSchema>;
}

export function AstronautDetailView({ dataItem }: AstronautDetailViewProps) {
  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold">{dataItem.name}</h2>
      <h3 className="text-xl font-medium mt-2">Status</h3>
      <p className="text-sm text-gray-400 mt-2">{dataItem.status.name}</p>
      <h3 className="text-xl font-medium mt-2">
        In Space: {dataItem.in_space ? "True" : "False"}
      </h3>
      {dataItem.in_space && dataItem.time_in_space ? (
        <h3 className="text-xl font-medium mt-2">
          Time in Space - {Duration.fromISO(dataItem.time_in_space).days} Days
        </h3>
      ) : null}
      <h3 className="text-xl font-medium mt-2">EVA Time</h3>
      <p className="text-sm text-gray-400 mt-2">{dataItem.eva_time}</p>
      <h3 className="text-xl font-medium mt-2">Agency</h3>
      <p className="text-sm text-gray-400 mt-2">{dataItem.agency.name}</p>
      <h3 className="text-xl font-medium mt-2">Bio</h3>
      <p className="text-sm text-gray-400 mt-2">{dataItem.bio}</p>
      <h3 className="text-xl font-medium mt-2">
        Flights Count- {dataItem.flights_count}
      </h3>
      <h3 className="text-xl font-medium mt-2">
        Landings Count - {dataItem.landings_count}
      </h3>
      <h3 className="text-xl font-medium mt-2">
        Spacewalks Count - {dataItem.spacewalks_count}
      </h3>
      <h3 className="text-xl font-medium mt-2">First Flight</h3>
      <p className="text-sm text-gray-400 mt-2">
        {DateTime.fromISO(dataItem.first_flight ?? "").toString()}
      </p>
      <h3 className="text-xl font-medium mt-2">Last Flight</h3>
      <p className="text-sm text-gray-400 mt-2">
        {DateTime.fromISO(dataItem.last_flight ?? "").toString()}
      </p>
    </div>
  );
}
