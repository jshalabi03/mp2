import { LaunchDetailSchema, LaunchSchema } from "@/api/schemas/launch";
import { ImageCard } from "@/components/image-card";
import { z } from "zod";
import { DateTime } from "luxon";

interface LaunchViewProps {
  dataItem: z.infer<typeof LaunchSchema>;
}

export function LaunchView({ dataItem }: LaunchViewProps) {
  return (
    <ImageCard
      imageSrc={dataItem.image}
      title={dataItem.name}
      description={dataItem.launch_service_provider.name}
      detailUrl={`${dataItem.id}`}
    >
      <div className="flex flex-col space-y-2">
        <div className="flex flex-row space-x-2">
          <span className="inline-block bg-blue-200 text-blue-800 p-2 radius">
            Status: {dataItem.status.name}
          </span>
        </div>
        <div className="flex flex-col space-y-2">
          <span className="inline-block bg-orange-200 text-orange-800 p-2 radius">
            Launch Start Time:{" "}
            {DateTime.fromISO(dataItem.window_start).toString()}
          </span>
          <span className="inline-block bg-orange-200 text-orange-800 p-2 radius">
            Launch End Time: {DateTime.fromISO(dataItem.window_end).toString()}
          </span>
        </div>
      </div>
    </ImageCard>
  );
}

interface LaunchDetailViewProps {
  dataItem: z.infer<typeof LaunchDetailSchema>;
}

export function LaunchDetailView({ dataItem }: LaunchDetailViewProps) {
  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold">{dataItem.name}</h2>
      <h2 className="mt-4 text-lg font-medium">
        Status: {dataItem.status.name}
      </h2>
      <p className="mt-2 text-sm text-gray-500">
        {dataItem.status.description}
      </p>
      <h2 className="mt-4 text-lg font-medium">
        Launch Probability: {dataItem.probability}
      </h2>
      <h2 className="mt-4 text-lg font-medium">
        Mission: {dataItem.mission.name}
      </h2>
      <p className="mt-2 text-sm text-gray-500">
        {dataItem.mission.description}
      </p>
      <h2 className="mt-4 text-lg font-medium">Pad: {dataItem.pad.name}</h2>
      <p className="mt-2 text-sm text-gray-500">{dataItem.pad.description}</p>
      <h2 className="mt-4 text-xl font-bold">Updates:</h2>
      <ul className="flex flex-col">
        {dataItem.updates.slice(0, 10).map((update) => {
          return (
            <li
              key={update.id}
              className="flex flex-col bg-white shadow-lg rounded-lg p-4 mb-4"
            >
              <div className="flex items-center space-x-2 text-base">
                <div className="mt-3 flex -space-x-2 overflow-hidden">
                  <img
                    className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
                    src={update.profile_image}
                    alt=""
                  />
                </div>
                <h4 className="font-semibold text-slate-900">
                  {update.created_by}
                </h4>
                <span className="rounded-full bg-slate-100 px-2 py-1 text-xs font-semibold text-slate-700">
                  {update.created_on}
                </span>
              </div>
              <p className="mt-2 text-sm text-gray-500">{update.comment}</p>
              <div className="mt-3 text-sm font-medium">
                <a
                  target="_blank"
                  href={update.info_url}
                  className="text-blue-500"
                >
                  More info
                </a>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
