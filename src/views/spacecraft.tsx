import {
  SpacecraftDetailSchema,
  SpacecraftSchema,
} from "@/api/schemas/spacecraft";
import { ImageCard } from "@/components/image-card";
import { z } from "zod";
import { Duration } from "luxon";

interface SpacecraftViewProps {
  dataItem: z.infer<typeof SpacecraftSchema>;
}

export function SpacecraftView({ dataItem }: SpacecraftViewProps) {
  return (
    <ImageCard
      imageSrc={dataItem.spacecraft_config.image_url}
      title={dataItem.name}
      description={dataItem.description}
      detailUrl={`${dataItem.id}`}
    >
      <div className="flex flex-col space-y-2">
        <div className="flex flex-row space-x-2">
          <span className="inline-block bg-blue-200 text-blue-800 p-2 radius">
            Status: {dataItem.status.name}
          </span>
        </div>
        <div className="flex flex-row space-x-2">
          <span className="inline-block bg-blue-200 text-blue-800 p-2 radius">
            Number of Flights: {dataItem.flights_count}
          </span>
          <span className="inline-block bg-blue-200 text-blue-800 p-2 radius">
            Number of Mission Ends: {dataItem.mission_ends_count}
          </span>
        </div>
        <div className="flex flex-row space-x-2">
          {dataItem.in_space ? (
            <>
              <span className="inline-block bg-indigo-400 text-white p-2 radius">
                Time In Space:{" "}
                {Duration.fromISO(dataItem.time_in_space).toHuman()}
              </span>
              <span className="inline-block bg-indigo-400 text-white p-2 radius">
                Time Docked: {Duration.fromISO(dataItem.time_docked).toHuman()}
              </span>
            </>
          ) : (
            <span className="inline-block bg-red-500 text-white p-2 radius">
              Not In Space
            </span>
          )}
        </div>
      </div>
    </ImageCard>
  );
}

interface SpacecraftDetailViewProps {
  dataItem: z.infer<typeof SpacecraftDetailSchema>;
}

export function SpacecraftDetailView({ dataItem }: SpacecraftDetailViewProps) {
  return (
    <div className="p-6">
      <h2 className="text-2xl font-bold">{dataItem.name}</h2>
      <h3 className="text-xl font-medium mt-2">History</h3>
      <p className="text-sm text-gray-400 mt-2">
        {dataItem.spacecraft_config.history}
      </p>
      <h3 className="text-xl font-medium mt-2">Details</h3>
      <p className="text-sm text-gray-400 mt-2">
        {dataItem.spacecraft_config.details}
      </p>
    </div>
  );
}
