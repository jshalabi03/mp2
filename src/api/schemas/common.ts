import { z } from "zod";

export const IdNameSchema = z.object({
  id: z.number(),
  name: z.string(),
});
