import { z } from "zod";

const SpacecraftSchema = z.object({
  id: z.number(),
  url: z.string(),
  name: z.string(),
  serial_number: z.string(),
  is_placeholder: z.boolean(),
  in_space: z.boolean(),
  time_in_space: z.string(),
  time_docked: z.string(),
  flights_count: z.number(),
  mission_ends_count: z.number(),
  status: z.object({
    id: z.number(),
    name: z.string(),
  }),
  description: z.string(),
  spacecraft_config: z.object({
    id: z.number(),
    url: z.string(),
    name: z.string(),
    type: z.object({
      id: z.number(),
      name: z.string(),
    }),
    agency: z.object({
      id: z.number(),
      url: z.string(),
      name: z.string(),
      type: z.string(),
    }),
    in_use: z.boolean(),
    image_url: z.string(),
  }),
});

const SpacecraftDetailSchema = z.object({
  id: z.number(),
  url: z.string(),
  name: z.string(),
  serial_number: z.string(),
  is_placeholder: z.boolean(),
  in_space: z.boolean(),
  time_in_space: z.string(),
  time_docked: z.string(),
  flights_count: z.number(),
  mission_ends_count: z.number(),
  status: z.object({
    id: z.number(),
    name: z.string(),
  }),
  description: z.string(),
  spacecraft_config: z.object({
    id: z.number(),
    url: z.string(),
    name: z.string(),
    type: z.object({
      id: z.number(),
      name: z.string(),
    }),
    agency: z.object({
      id: z.number(),
      url: z.string(),
      name: z.string(),
      type: z.string(),
    }),
    in_use: z.boolean(),
    image_url: z.string(),
    history: z.string(),
    details: z.string(),
  }),
  flights: z.array(
    z.object({
      id: z.number(),
      url: z.string(),
      destination: z.string(),
      mission_end: z.string(),
      spacecraft: z.object({
        id: z.number(),
        url: z.string(),
        name: z.string(),
        serial_number: z.string(),
        is_placeholder: z.boolean(),
        in_space: z.boolean(),
        time_in_space: z.string(),
        time_docked: z.string(),
        flights_count: z.number(),
        mission_ends_count: z.number(),
        status: z.object({
          id: z.number(),
          name: z.string(),
        }),
        description: z.string(),
        spacecraft_config: z.object({
          id: z.number(),
          url: z.string(),
          name: z.string(),
          type: z.object({
            id: z.number(),
            name: z.string(),
          }),
          agency: z.object({
            id: z.number(),
            url: z.string(),
            name: z.string(),
            type: z.string(),
          }),
          in_use: z.boolean(),
          image_url: z.string(),
        }),
      }),
    })
  ),
});

export { SpacecraftDetailSchema, SpacecraftSchema };
