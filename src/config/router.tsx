import {
  LoaderFunctionArgs,
  Navigate,
  createBrowserRouter,
  defer,
} from "react-router-dom";
import { MainLayout } from "@/components/main-layout";
import { views } from "./views";
import { DetailViewRenderer, ViewRenderer } from "@/lib/view-renderer";
import { queryClient } from "@/lib/react-query";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: views
      .map((view) => {
        return {
          path: view.pageRoute,
          element: <ViewRenderer view={view} />,
          children: [
            {
              path: ":id",
              element: <DetailViewRenderer view={view} />,
              loader: async ({ params }: LoaderFunctionArgs) => {
                const dataPromise = queryClient.fetchQuery(
                  [view.id, params.id],
                  () =>
                    fetch(`${view.endpointPath}/${params.id}`).then((res) =>
                      res.json()
                    )
                );
                return defer({
                  dataItem: dataPromise,
                });
              },
            },
          ],
        };
      })
      .concat({
        path: "/",
        element: <Navigate to={views[0].pageRoute} replace={true} />,
        children: [],
      }),
  },
]);
