/**
 * views.ts
 *
 * This file contains all the data required to define
 * each view for an API response, including how the data
 * can be fetched, how to render fetched data, where to
 * route the view, etc.
 */

import React from "react";
import { z } from "zod";

/**
 * Schemas
 */
import {
  SpacecraftDetailSchema,
  SpacecraftSchema,
} from "@/api/schemas/spacecraft";
import { LaunchDetailSchema, LaunchSchema } from "@/api/schemas/launch";
import {
  AstronautDetailedSchema,
  AstronautSchema,
} from "@/api/schemas/astronaut";

/**
 * Views
 */
import { SpacecraftView, SpacecraftDetailView } from "@/views/spacecraft";
import { LaunchView, LaunchDetailView } from "@/views/launch";
import { AstronautView, AstronautDetailView } from "@/views/astronaut";

import { BASE_URL } from "@/api";

export type Field = {
  field: string;
  label: string;
};

export type ViewType = "spacecrafts" | "launches" | "astronauts";

type View<T extends z.ZodTypeAny, TDetail extends z.ZodTypeAny> = {
  id: number;
  viewType: ViewType;
  pageRoute: string;
  title: string;
  description: string;
  isSearchable: boolean;
  filterOptions: Array<Field>;
  orderingFields: Array<Field>;
  endpointPath: string;
  responseSchema: T;
  dataComponent: React.FC<{ dataItem: z.infer<T> }>;
  detailSchema: TDetail;
  detailComponent: React.FC<{ dataItem: z.infer<TDetail> }>;

  /**
   * For extensibility purposes,
   * it may be better to explicitly
   * define a detail loader function
   * per view, i.e.,
   *
   * detailLoader: ({ params }: LoaderFunctionArgs<any>) => Promise<TDetail>;
   *
   */
};

export type ViewObject = View<z.ZodTypeAny, z.ZodTypeAny>;

export const views: Array<ViewObject> = [
  {
    id: 0,
    viewType: "spacecrafts",
    pageRoute: "/spacecrafts",
    title: "Spacecrafts",
    description:
      "Explore all spacecraft configurations past, present and future.",
    isSearchable: true,
    filterOptions: [{ field: "in_space", label: "In Space" }],
    orderingFields: [
      { field: "time_in_space", label: "Time in Space" },
      { field: "time_docked", label: "Time Docked" },
      { field: "flights_count", label: "Flights Count" },
      { field: "mission_ends_count", label: "Mission Ends Count" },
    ],
    endpointPath: `${BASE_URL}/spacecraft`,
    responseSchema: SpacecraftSchema,
    dataComponent: SpacecraftView,
    detailSchema: SpacecraftDetailSchema,
    detailComponent: SpacecraftDetailView,
  },
  {
    id: 1,
    viewType: "launches",
    pageRoute: "/launches",
    title: "Launches",
    description:
      "Explore all orbital launches and a number of popular suborbital launches",
    isSearchable: true,
    filterOptions: [{ field: "is_crewed", label: "Is Crewed" }],
    orderingFields: [],
    endpointPath: `${BASE_URL}/launch`,
    responseSchema: LaunchSchema,
    dataComponent: LaunchView,
    detailSchema: LaunchDetailSchema,
    detailComponent: LaunchDetailView,
  },
  {
    id: 2,
    viewType: "astronauts",
    pageRoute: "/astronauts",
    title: "Astronauts",
    description: "Explore every known astronaut past, present and future",
    isSearchable: true,
    filterOptions: [
      {
        field: "in_space",
        label: "In Space",
      },
      {
        field: "has_flown",
        label: "Has Flown",
      },
      {
        field: "is_human",
        label: "Is Human",
      },
    ],
    orderingFields: [
      {
        field: "name",
        label: "Name",
      },
      {
        field: "status",
        label: "Status",
      },
      {
        field: "date_of_birth",
        label: "Date of Birth",
      },
    ],
    endpointPath: `${BASE_URL}/astronaut`,
    responseSchema: AstronautSchema,
    dataComponent: AstronautView,
    detailSchema: AstronautDetailedSchema,
    detailComponent: AstronautDetailView,
  },
];

export type Views = typeof views;
