import { views } from "./views";

interface NavItem {
  path: string;
  label: string;
}

export const navItems: Array<NavItem> = views.map((view) => {
  return {
    path: view.pageRoute,
    label: view.title,
  };
});
