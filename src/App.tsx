import { QueryClientProvider } from "react-query";
import { queryClient } from "@/lib/react-query";
import { RouterProvider } from "react-router-dom";

import { router } from "./config/router";

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  );
}

export default App;
